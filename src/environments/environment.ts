// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://irychkov.myqnapcloud.com:8888/api',
  apiMap: {
    pushSubscribe: '/push/subscribe',
    searchApartments: '/apartments/search'
  },
  vapidKeys: {
    publicKey: 'BCzk_La540h0JYvp_aco7affvZvRrqB7YRgnXw4t9kb_S83Iq6ff238hOlG6Yr23HpoYUHOCjD-rA8Fzntnqkw0',
    privateKey: 'IOQHRkTulmxYFyTuq5mMMIG3wD9sfjsKZQbSXgoZMuE'
  },
  mapsApiKey: 'AIzaSyAztubjIjPvA-7v4JiHBmL9_nbd-KqmXCg'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
