import {Component, NgZone, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm} from '@angular/forms';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../environments/environment';
import {MatDrawer, MatSnackBar} from '@angular/material';
import {MapsAPILoader} from '@agm/core';
import {google} from '@agm/core/services/google-maps-types';
import {SwPush} from '@angular/service-worker';
import {PushService} from './push.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {
  isLoading = true;
  private defaultCity = 'minsk';
  private requestParams: HttpParams;
  private cityMapParameters = {
    'minsk': {
      bounds: {
        lb: {
          lat: 53.706055741812136,
          long: 27.235107421875004
        },
        rt: {
          lat: 54.08920096406033,
          long: 27.888793945312504
        }
      },
      zoom: 10,
      hasMetro: true
    },
    'brest': {
      bounds: {
        lb: {
          lat: 51.988263083821714,
          long: 23.546447753906254
        },
        rt: {
          lat: 52.1880361948174,
          long: 23.873291015625
        }
      },
      zoom: 11
    },
    'vitebsk': {
      bounds: {
        lb: {
          lat: 55.12904164631515,
          long: 30.03318786621094
        },
        rt: {
          lat: 55.3144939753665,
          long: 30.36003112792969
        }
      },
      zoom: 11
    },
    'gomel': {
      bounds: {
        lb: {
          lat: 52.33412852783782,
          long: 30.76150413122697
        },
        rt: {
          lat: 52.56175261410684,
          long: 31.13694844830516
        }
      },
      zoom: 10
    },
    'grodno': {
      bounds: {
        lb: {
          lat: 53.59558218137305,
          long: 23.704229062114095
        },
        rt: {
          lat: 53.76322681396814,
          long: 23.9887626477129
        }
      },
      zoom: 11
    },
    'mogilev': {
      bounds: {
        lb: {
          lat: 53.78746901588889,
          long: 30.185623168945316
        },
        rt: {
          lat: 53.979108639593605,
          long: 30.512466430664066
        }
      },
      zoom: 11
    }
  };
  currentCurrency = 'usd';
  currencies = [
    {name: 'Цены в рублях', value: 'byn'},
    {name: 'Цены в долларах', value: 'usd'}
  ];
  rooms = [
    {name: '1', value: '1_room'},
    {name: '2', value: '2_rooms'},
    {name: '3', value: '3_rooms'},
    {name: '4+', value: '4_rooms'}
  ];
  cities = [
    {name: 'Минск', value: 'minsk'},
    {name: 'Брест', value: 'brest'},
    {name: 'Витебск', value: 'vitebsk'},
    {name: 'Гомель', value: 'gomel'},
    {name: 'Гродно', value: 'grodno'},
    {name: 'Могилёв', value: 'mogilev'}
  ];
  metros = [
    {name: 'Не важно', value: ''},
    {name: 'Возле метро', value: 'two_lines'},
    {name: 'Москвоская линия', value: 'blue_line'},
    {name: 'Автозаводская линия', value: 'red_line'}
  ];
  userLocation: any = null;
  currentLat: number = null;
  currentLong: number = null;
  currentZoom: number = null;
  currentPage = 1;
  settings: any;
  private defaultSettings: any = {
    city: this.defaultCity
  };
  settingsForm: FormGroup;
  searchResults: any;
  currentSortType = 'last_time_up:desc';
  sortTypes = [
    {name: 'Сначала актуальные', value: 'last_time_up:desc'},
    {name: 'Сначала новые', value: 'created_at:desc'},
    {name: 'Сначала дешёвые', value: 'price:asc'},
    {name: 'Сначала дорогие', value: 'price:desc'}
  ];
  showMap = false;
  focusedApartment: number = null;
  scrollToTopVisibility = false;

  @ViewChild(MatDrawer) drawer: MatDrawer;

  constructor(
      private fb: FormBuilder,
      private http: HttpClient,
      private snackBar: MatSnackBar,
      private swPush: SwPush,
      private pushService: PushService,
      private mapsAPILoader: MapsAPILoader,
      private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.createSettingsForm();
  }

  ngAfterViewInit() {
    window.addEventListener('scroll', this.scrollFunction, {passive: true});
    if (!localStorage.getItem('pushProposed')) {
      localStorage.setItem('pushProposed', 'true');
      this.swPush.requestSubscription({
        serverPublicKey: environment.vapidKeys.publicKey
      })
      .then(response => this.pushService.addPushSubscriber(response).subscribe())
      .catch(error => {
        console.error('Could not subscribe to notifications', error);
      });
    }
    this.getCurrentPosition();
  }

  private getCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.userLocation = {
          lat: position.coords.latitude,
          long: position.coords.longitude,
        };
      });
    }
  }

  private createSettingsForm() {
    const that = this;
    this.settingsForm = this.fb.group({
      'type': null,
      'rooms': this.fb.array([]),
      'priceMin': 50,
      'priceMax': 7000,
      'onlyOwner': null,
      'city': this.defaultCity,
      'metro': null,
      'address': null
    });
    if (localStorage.getItem('search_settings')) {
      this.settings = JSON.parse(localStorage.getItem('search_settings'));
      this.settingsForm.patchValue(this.settings);
      this.showCity(this.settings.city);
    } else {
      this.resetSettings();
      this.isLoading = false;
    }
    this.currentPage = 1;
    this.generateRequestParams();
    this.getResults();
  }

  resetSettings() {
    this.currentPage = 1;
    this.showCity(this.defaultCity);
    this.settings = this.defaultSettings;
  }

  onRentTypeChange(value: string, checked: boolean) {
    const roomsFormArray = <FormArray>this.settingsForm.controls['rooms'];
    if (value === 'room' && checked) {
      const values = ['1_room', '2_rooms', '3_rooms', '4_rooms', '5_rooms', '6_rooms'];
      values.map(el => {
        const index = roomsFormArray.controls.findIndex(x => x.value === el);
        if (index) {
          roomsFormArray.removeAt(index);
        }
      });
      roomsFormArray.push(new FormControl(value));
    } else {
      const index = roomsFormArray.controls.findIndex(x => x.value === 'room');
      roomsFormArray.removeAt(index);
    }
  }

  onRoomsChange(value: string, checked: boolean) {
    const roomsFormArray = <FormArray>this.settingsForm.controls['rooms'];
    const values = [];
    if (value === '4_rooms') {
      values.push('4_rooms', '5_rooms', '6_rooms');
    } else {
      values.push(value);
    }
    if (checked) {
      values.map(el => {
        roomsFormArray.push(new FormControl(el));
      });
    } else {
      values.map(el => {
        const index = roomsFormArray.controls.findIndex(x => x.value === el);
        roomsFormArray.removeAt(index);
      });
    }
    console.log(roomsFormArray);
  }

  onCityChange(event: any) {
    this.showCity(event.value);
  }

  showCity(city: string) {
    this.currentLat = null;
    this.currentLong = null;
    this.currentZoom = null;
    if (city && this.cityMapParameters[city]) {
      this.currentLat = (this.cityMapParameters[city].bounds.lb.lat + this.cityMapParameters[city].bounds.rt.lat) / 2;
      this.currentLong = (this.cityMapParameters[city].bounds.lb.long + this.cityMapParameters[city].bounds.rt.long) / 2;
      this.currentZoom = this.cityMapParameters[city].zoom;
    }
  }

  onSubmit(form: NgForm) {
    this.settings = JSON.parse(JSON.stringify(form));
    localStorage.setItem('search_settings', JSON.stringify(this.settings));
    this.generateRequestParams();
    this.snackBar.open('Сохранено', null, {
      duration: 2000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
    this.drawer.toggle();
    this.getResults();
  }

  private generateRequestParams() {
    if (!this.settings) {
      this.currentPage = 1;
      this.settings = this.defaultSettings;
    }
    this.requestParams = new HttpParams();
    if (this.settings.city && this.cityMapParameters[this.settings.city]) {
      this.requestParams = this.requestParams.append('bounds[lb][lat]', this.cityMapParameters[this.settings.city].bounds.lb.lat);
      this.requestParams = this.requestParams.append('bounds[lb][long]', this.cityMapParameters[this.settings.city].bounds.lb.long);
      this.requestParams = this.requestParams.append('bounds[rt][lat]', this.cityMapParameters[this.settings.city].bounds.rt.lat);
      this.requestParams = this.requestParams.append('bounds[rt][long]', this.cityMapParameters[this.settings.city].bounds.rt.long);
      if (this.cityMapParameters[this.settings.city].hasMetro) {
        if (this.settings.metro === 'all_lines') {
          this.requestParams = this.requestParams.append('metro[]', 'red_line');
          this.requestParams = this.requestParams.append('metro[]', 'blue_line');
        } else if (this.settings.metro) {
          this.requestParams = this.requestParams.append('metro[]', this.settings.metro);
        }
      }
    }
    if (this.settings.priceMin && this.settings.priceMin !== 50 && this.settings.priceMax && this.settings.priceMax !== 7000 && this.settings.priceMin <= this.settings.priceMax) {
      this.requestParams = this.requestParams.append('price[min]', this.settings.priceMin);
      this.requestParams = this.requestParams.append('price[max]', this.settings.priceMax);
    }
    if (this.settings.rooms) {
      this.settings.rooms.map(param => {
        this.requestParams = this.requestParams.append('rent_type[]', param);
      });
    }
    if (this.settings.onlyOwner) {
      this.requestParams = this.requestParams.append('only_owner', 'true');
    }
    this.requestParams = this.requestParams.append('page', this.currentPage.toString());
  }

  getResults() {
    this.isLoading = true;
    this.http.get(environment.apiUrl + environment.apiMap.searchApartments, {params: this.requestParams}).subscribe((response: any) => {
      if (response.apartments && response.page) {
        this.searchResults = response;
      }
      this.isLoading = false;
    }, (error) => {
      this.isLoading = false;
      this.snackBar.open('Ошибка. Проверьте ваше Интернет-соединение', null, {
        duration: 2000,
        horizontalPosition: 'right',
        verticalPosition: 'top'
      });
    });
  }

  onSortTypeChange(event: any) {
    this.currentSortType = event.value;
    this.requestParams = this.requestParams.set('order', this.currentSortType);
    this.requestParams = this.requestParams.set('page', '1');
    this.getResults();
  }

  onCurrencyChange(event: any) {
    this.currentCurrency = event.value;
    this.requestParams = this.requestParams.set('currency', this.currentCurrency);
    this.getResults();
  }

  getSubtitle(rentType: string) {
    let name: string;
    if (rentType === 'room') {
      name = 'Комната';
    } else {
      name = rentType.slice(0, 1) + '-комнатная квартира';
    }
    return name;
  }

  getPhotoAlt(rentType: string, address: string) {
    return `${this.getSubtitle(rentType)} на ${address}`;
  }

  getPrice(data: any) {
    let price: string = data.converted[this.currentCurrency.toUpperCase()].amount.toString();
    switch (this.currentCurrency) {
      case 'usd':
        price = '$' + price;
        break;
      case 'byn':
        price = price + ' BYN';
        break;
    }
    return price;
  }

  scrollToApartment(id) {
    const e = document.getElementById(`apartment${id}`);
    if (!!e && e.scrollIntoView) {
      e.scrollIntoView({block: 'center', behavior: 'smooth'});
    }
    this.focusedApartment = id;
    setTimeout(() => {
      if (this.focusedApartment = id) {
        this.focusedApartment = null;
      }
    }, 3000);
  }

  scrollToTop() {
    const e = document.getElementById('top');
    if (!!e && e.scrollIntoView) {
      e.scrollIntoView({block: 'start', behavior: 'smooth'});
    }
  }

  scrollFunction(event: any) {
    const button = document.getElementById('scrollToTop');
    if (button) {
      if (window.scrollY > 300) {
        if (button.classList.contains('hidden')) {
          button.style.display = 'block';
          setTimeout(() => {
            button.classList.remove('hidden');
          }, 400);
        }
      } else {
        if (!button.classList.contains('hidden')) {
          button.classList.add('hidden');
          setTimeout(() => {
            button.style.display = 'block';
          }, 400);
        }
      }
    }
  }

  pagination(event: any) {
    this.currentPage = event.pageIndex + 1;
    this.isLoading = true;
    this.generateRequestParams();
    this.getResults();
  }
}
