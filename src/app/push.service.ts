import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PushService {

  constructor(private http: HttpClient) { }

  addPushSubscriber(subscriber: any) {
    return this.http.post(environment.apiUrl + environment.apiMap.pushSubscribe, subscriber);
  }
}
